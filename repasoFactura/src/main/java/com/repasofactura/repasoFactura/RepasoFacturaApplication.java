package com.repasofactura.repasoFactura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepasoFacturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepasoFacturaApplication.class, args);
	}

}
